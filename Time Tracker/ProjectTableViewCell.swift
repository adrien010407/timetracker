//
//  ProjectTableViewCell.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 10/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {

    //MARK: Outlet
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
