//
//  Project+.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 11/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit

extension Project {
    func getTotalTime() -> TimeInterval {
        var interval = TimeInterval(0)
        for task in tasks?.allObjects as! [Task]? ?? [] {
            interval += task.getTotalTime()
        }
        return interval
    }
    
    func hasStarted() -> Bool {
        var started: Bool = false
        for task in tasks?.allObjects as! [Task] {
            started = started || task.hasStarted()
        }
        return started
    }
}
