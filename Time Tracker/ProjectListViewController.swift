//
//  ViewController.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 13/03/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData
import os.log

class ProjectListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
    
    var selectedIndexPath: IndexPath?
    
    struct MainSection {
        var type: String
        var contents: [AnyObject]
    }
    
    class ButtonData {
        let image: UIImage
        let text: String
        let doAction: (_ sender: Any?) -> ()
        let getTime: () -> (TimeInterval)
        
        init(image: UIImage, text: String, doAction: @escaping (_ sender: Any?) -> (), getTime: @escaping () -> (TimeInterval)) {
            self.image = image
            self.text = text
            self.doAction = doAction
            self.getTime = getTime
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    let tasksIndex = 0
    let projectsIndex = 2
    var sections = [
        MainSection(type: "tasks", contents: []),
        MainSection(type: "buttons", contents: []),
        MainSection(type: "projects", contents: [])
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loadData()
        sections[1].contents.removeAll()
        sections[1].contents.append(ButtonData(image: UIImage(named: "files")!, text: "Toutes les tâches", doAction: {(sender) in
            self.performSegue(withIdentifier: "showTasks", sender: sender)
        }, getTime: {() in TimeInterval(0)}))
        sections[1].contents.append(ButtonData(image: UIImage(named: "file")!, text: "Tâches standalone", doAction: {(sender) in
            self.performSegue(withIdentifier: "showSingleTasks", sender: sender)
        }, getTime: {() in TimeInterval(0)}))
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ProjectListViewController.refreshTableView), userInfo: nil, repeats: true)
    }
    
    func loadData() -> Void {
        loadProjectsData()
        loadTasksData()
    }
    
    func loadProjectsData() -> Void {
        let requestProjects : NSFetchRequest<Project> = Project.fetchRequest()
        do {
            sections[projectsIndex].contents.removeAll()
            sections[projectsIndex].contents.append(contentsOf: try context.fetch(requestProjects))
        } catch {
            print("Les données n'ont pas pu être chargé")
        }
    }
    
    func loadTasksData() -> Void {
        let requestTasks : NSFetchRequest<Task> = Task.fetchRequest()
        requestTasks.predicate = NSPredicate(format: "(periods.@count != 0) AND (ANY periods.end == nil)")
        do {
            sections[tasksIndex].contents.removeAll()
            sections[tasksIndex].contents.append(contentsOf: try context.fetch(requestTasks))
        } catch {
            print("Les données n'ont pas pu être chargé")
        }
    }
    
    //MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        return section.contents.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch sections[section].type {
        case "tasks":
            return "Tâches"
        case "projects":
            return "Projets"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ProjectCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProjectTableViewCell else {
            fatalError("La cellule n'est pas une instance de ProjectTableViewCell")
        }
        switch sections[indexPath.section].type {
        case "projects":
            let project = sections[indexPath.section].contents[indexPath.row] as! Project
            let interval: TimeInterval
            if project.tasks != nil && project.tasks!.count > 0 {
                interval = project.getTotalTime()
            } else {
                interval = TimeInterval(0)
            }
            cell.leftImage.image = UIImage(named: "Folder")
            cell.nameLabel.text = project.name
            cell.timeLabel.text = (tableView.isEditing) ? "" : interval.getTimeString()
            if project.hasStarted() {
                cell.nameLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
                cell.timeLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            } else {
                cell.nameLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
                cell.timeLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            }
        case "buttons":
            guard let button = sections[indexPath.section].contents[indexPath.row] as? ButtonData else {
                fatalError("La cellule n'est pas du type button")
            }
            cell.leftImage.image = button.image
            cell.nameLabel.text = button.text
            cell.timeLabel.text = ""
            cell.nameLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            cell.timeLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        case "tasks":
            let task = sections[indexPath.section].contents[indexPath.row] as! Task
            let interval: TimeInterval = task.getTotalTime()
            cell.nameLabel.text = task.name
            cell.timeLabel.text = (tableView.isEditing) ? "" :interval.getTimeString()
            if task.hasStarted() {
                if task.quickTask {
                    cell.leftImage.image = UIImage(named: "quickPause")
                } else {
                    cell.leftImage.image = UIImage(named: "pause")
                }
                cell.nameLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
                cell.timeLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            } else {
                cell.leftImage.image = UIImage(named: "play")
                cell.nameLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
                cell.timeLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            }
        default:
            fatalError("La cellule n'est pas d'un type connu")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Suppression de la ligne
            switch sections[indexPath.section].type {
            case "projects":
                context.delete(sections[indexPath.section].contents[indexPath.row] as! Project)
                sections[indexPath.section].contents.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                do {
                    try context.save()
                } catch {
                    fatalError("La suppression a échoué")
                }
            case "tasks":
                context.delete(sections[indexPath.section].contents[indexPath.row] as! Task)
                sections[indexPath.section].contents.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                do {
                    try context.save()
                } catch {
                    fatalError("La suppression a échoué")
                }
            default:
                fatalError("La ligne ne devrait pas être supprimable")
            }
        } else if editingStyle == .insert {
            if #available(iOS 10.0, *) {
                os_log("Insertion Non Implémenter", log: OSLog.default, type: .error)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch sections[indexPath.section].type {
        case "projects", "tasks":
            return true
        default:
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        if sections[indexPath.section].type == "projects" {
            performSegue(withIdentifier: "ShowProjectDetail", sender: tableView.cellForRow(at: indexPath))
        } else if sections[indexPath.section].type == "tasks" {
            let task = sections[indexPath.section].contents[indexPath.row] as! Task
            if task.quickTask {
                performSegue(withIdentifier: "presentQuickTaskMenu", sender: tableView.cellForRow(at: indexPath))
            } else {
                if tableView.isEditing {
                    performSegue(withIdentifier: "ShowTaskDetail", sender: tableView.cellForRow(at: indexPath))
                } else {
                    if task.hasStarted() {
                        task.stop()
                    } else {
                        task.start()
                    }
                    tableView.reloadData()
                }
            }
        } else if sections[indexPath.section].type == "buttons" {
            let button = sections[indexPath.section].contents[indexPath.row] as! ButtonData
            button.doAction(tableView.cellForRow(at: indexPath))
        }
    }
    
    //MARK: Actions
    @IBAction func addQuickTask(_ sender: UIButton) {
        let quickTask: Task
        if #available(iOS 10.0, *) {
            quickTask = Task(context: context)
        } else {
            let entityDescription = NSEntityDescription.entity(forEntityName: "Task", in: context)
            quickTask = Task(entity: entityDescription!, insertInto: context)
        }
        quickTask.name = "Tâche rapide"
        quickTask.quickTask = true
        quickTask.start()
        
        do {
            try context.save()
            print("Tâche sauvegardé")
        } catch {
            print("Erreur durant la sauvegarde : \(error)")
        }
        
        loadTasksData()
        tableView.reloadData()
    }
    
    @IBAction func toggleEditMode(_ sender: Any) {
        if (tableView.isEditing) {
            tableView.isEditing = false
            navigationItem.rightBarButtonItem?.title = "Modifier"
        }
        else {
            tableView.isEditing = true
            navigationItem.rightBarButtonItem?.title = "Sauvegarder"
            tableView.reloadData()
        }
    }
    
    @IBAction func unwindToProjectList(_ unwindSegue: UIStoryboardSegue) {
        if let sourceViewController = unwindSegue.source as? ProjectDetailViewController, let project = sourceViewController.project {
            if (sourceViewController.mode == .DETAIL || sourceViewController.mode == .EDIT) && selectedIndexPath != nil {
                sections[selectedIndexPath!.section].contents[selectedIndexPath!.row] = project
            } else {
                sections[projectsIndex].contents.append(project)
                print("Sauvegarde réussie")
            }
        }
        loadTasksData()
        tableView.reloadData()
        // Use data from the view controller which initiated the unwind segue
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "AddProject", "showTasks":
            break
        case "presentQuickTaskMenu":
            guard let quickTaskMenuViewController = segue.destination as? QuickTaskMenuViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedTask = sections[indexPath.section].contents[indexPath.row] as! Task
            quickTaskMenuViewController.unwindSegueIdentifier = "unwindToProjectList"
            quickTaskMenuViewController.quickTask = selectedTask
        case "ShowTaskDetail":
            guard let taskDetailViewController = segue.destination as? TaskDetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedTask = sections[indexPath.section].contents[indexPath.row] as! Task
            taskDetailViewController.unwindSegueIdentifier = "unwindToProjectList"
            taskDetailViewController.task = selectedTask
            taskDetailViewController.mode = .DETAIL
        case "showSingleTasks":
            guard let tasksTableViewController = segue.destination as? TasksTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            tasksTableViewController.single = true
        case "ShowProjectDetail":
            guard let projectDetailViewController = segue.destination as? ProjectDetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedProjectCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedProjectCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedProject = sections[indexPath.section].contents[indexPath.row] as! Project
            projectDetailViewController.project = selectedProject
        default:
            fatalError("Unexpected Segue Identifier: \(String(describing: segue.identifier))")
        }
    }
    
    //MARK: Functions
    
    @objc func refreshTableView() {
        if !tableView.isEditing {
            tableView.reloadData()
        }
    }
}

