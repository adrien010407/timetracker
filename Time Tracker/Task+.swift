//
//  File.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 10/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData

extension Task {
    // Retourne la dernière période d'une tâche
    func getLastPeriod() -> Period? {
        if (periods == nil || periods?.count == 0) {
            return nil
        }
        return (periods?.allObjects as! [Period]).sorted(by: {$0.start! > $1.start!})[0]
    }
    
    // Indique si la tâche est en cours de réalisation, c'est à dire si une nouvelle période a démarrée
    func hasStarted() -> Bool {
        let lastPeriod = getLastPeriod()
        return (lastPeriod != nil && lastPeriod!.end == nil)
    }
    
    // Retourne l'interval de temps total passé sur la tâche
    func getTotalTime() -> TimeInterval {
        var interval = TimeInterval(0)
        for period in periods?.allObjects as! [Period]? ?? [] {
            interval += (period.end ?? Date()).timeIntervalSince((period.start)!)
        }
        return interval
    }
    
    // Indique qu'on recommence à travailler sur la tâche
    func start() {
        let newPeriod: Period
        if #available(iOS 10.0, *) {
            newPeriod = Period(context: managedObjectContext!)
        } else {
            let entityDescription = NSEntityDescription.entity(forEntityName: "Period", in: managedObjectContext!)
            newPeriod = Period(entity: entityDescription!, insertInto: managedObjectContext!)
        }
        newPeriod.start = Date()
        
        addToPeriods(newPeriod)
        
        do {
            try managedObjectContext!.save()
            print("Tâche démarrée")
        } catch {
            print("Erreur durant la sauvegarde : \(error)")
        }
    }
    
    // Indique qu'on arrête de travailler sur la tâche, on créer autant de tâche qu'il faut pour avoir des tâches qui ne chevauchent pas plusieurs jours différents
    func stop() {
        if var lastPeriod = getLastPeriod() {
            let currentDate = Date()
            let calendar = Calendar.current
            
            var startDate = lastPeriod.start!
            var endDay = calendar.date(bySetting: .nanosecond, value: 999999999, of: calendar.date(bySettingHour: 23, minute: 59, second: 59, of: startDate)!)
            while currentDate > endDay! {
                lastPeriod.end = endDay
                if #available(iOS 10.0, *) {
                    lastPeriod = Period(context: managedObjectContext!)
                } else {
                    let entityDescription = NSEntityDescription.entity(forEntityName: "Period", in: managedObjectContext!)
                    lastPeriod = Period(entity: entityDescription!, insertInto: managedObjectContext!)
                }
                let newStartDate = calendar.date(byAdding: .day, value: 1, to: calendar.date(bySetting: .nanosecond, value: 0, of: calendar.date(bySettingHour: 0, minute: 0, second: 0, of: startDate)!)!)
                startDate = newStartDate!
                lastPeriod.start = newStartDate!
                addToPeriods(lastPeriod)
                endDay = calendar.date(bySetting: .nanosecond, value: 999999999, of: calendar.date(bySettingHour: 23, minute: 59, second: 59, of: startDate)!)
            }
            
            lastPeriod.end = Date()
            
            do {
                try managedObjectContext!.save()
                print("Tâche stoppée")
            } catch {
                print("Erreur durant la sauvegarde : \(error)")
            }
        }
    }
}
