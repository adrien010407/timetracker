//
//  EditProjectViewController.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 13/03/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData
import os.log

class ProjectDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var context = (UIApplication.shared.delegate as! AppDelegate).databaseContext

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var selectedIndexPath: IndexPath?
    
    var tasksData = [Task]()
    @IBOutlet weak var tableView: UITableView!
    
    var project: Project?
    
    enum Mode {
        case UNKNOWN
        case ADD
        case DETAIL
        case EDIT
    }
    var mode: Mode = Mode.UNKNOWN
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let isPresentingInAddProjectMode = presentingViewController is UINavigationController
        
        if isPresentingInAddProjectMode {
            switchToAddMode()
        }
        else if navigationController != nil {
            switchToDetailMode()
        }
        else {
            fatalError("Le ProjectDetailViewController n'est pas dans un NavigationController")
        }
        
        if let project = project {
            navigationItem.title = project.name
            nameTextField.text = project.name
            
            loadData()
        }
        else {
            if #available(iOS 10.0, *) {
                project = Project(context: context)
            } else {
                let entityDescription = NSEntityDescription.entity(forEntityName: "Project", in: context)
                project = Project(entity: entityDescription!, insertInto: context)
            }
        }
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ProjectDetailViewController.refreshTableView), userInfo: nil, repeats: true)
    }
    
    func loadData() -> Void {
        if let project = project {
            let request : NSFetchRequest<Task> = Task.fetchRequest()
            request.predicate = NSPredicate(format: "project == %@", project)
            do {
                tasksData = try context.fetch(request)
            } catch {
                print("Les données n'ont pas pu être chargé")
            }
        }
    }
    
    //MARK: Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasksData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TaskCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TaskTableViewCell else {
            fatalError("La cellule n'est pas une instance de TaskTableViewCell")
        }
        cell.indexPath = indexPath
        let task = tasksData[indexPath.row]
        let interval: TimeInterval
        if task.periods != nil && task.periods!.count > 0 {
            interval = task.getTotalTime()
        } else {
            interval = TimeInterval(0)
        }
        
        cell.nameLabel.text = task.name
        cell.timeLabel.text = (mode == .ADD || mode == .EDIT) ? "" : interval.getTimeString()
        if task.hasStarted() {
            cell.leftImageView.image = UIImage(named: "pause")
            cell.leftImageView.tintColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            cell.nameLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            cell.timeLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            
        } else {
            cell.leftImageView.image = UIImage(named: "play")
            cell.leftImageView.tintColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            cell.nameLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            cell.timeLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }
        cell.historyButton?.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Suppression de la ligne
            context.delete(tasksData[indexPath.row])
            tasksData.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            do {
                try context.save()
            } catch {
                fatalError("La suppression a échoué")
            }
        } else if editingStyle == .insert {
            if #available(iOS 10.0, *) {
                os_log("Insertion Non Implémenter", log: OSLog.default, type: .error)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        if mode == .EDIT || mode == .ADD {
            performSegue(withIdentifier: "ShowTaskDetail", sender: tableView.cellForRow(at: indexPath))
        } else {
            let task = tasksData[indexPath.row]
            if task.hasStarted() {
                task.stop()
            } else {
                task.start()
            }
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        switch mode {
        case .ADD:
            dismiss(animated: true, completion: nil)
        case .DETAIL:
            performSegue(withIdentifier: "unwindToProjectList", sender: sender)
        case .EDIT:
            nameTextField.text = project!.name
            switchToDetailMode()
        default:
            fatalError("Le ProjectDetailViewController n'est pas dans un mode connu")
        }
    }
    
    @IBAction func saveOrEdit(_ sender: Any) {
        switch mode {
        case .ADD: // On sauvegarde un nouveau projet
            save()
            performSegue(withIdentifier: "unwindToProjectList", sender: sender)
        case .EDIT: // On sauvegarde une modification de projet
            save()
            switchToDetailMode()
        case .DETAIL: // On passe en mode édition
            switchToEditMode()
        default:
            fatalError("Le ProjectDetailViewController n'est pas dans un mode connu")
        }
    }
    
    //MARK: Fonctions
    private func save() {
        let name = nameTextField.text ?? "Projet sans nom"
        
        project?.name = name
        navigationItem.title = name
        
        do {
            try context.save()
            print("Projet sauvegardé")
        } catch {
            print("Erreur durant la sauvegarde : \(error)")
        }
    }
    
    @objc func refreshTableView() {
        if mode == .DETAIL {
            tableView.reloadData()
        }
    }
    
    @IBAction func unwindToProjectDetail(_ unwindSegue: UIStoryboardSegue) {
        if let sourceViewController = unwindSegue.source as? TaskDetailViewController, let task = sourceViewController.task {
            if sourceViewController.mode == .DETAIL || sourceViewController.mode == .EDIT, selectedIndexPath != nil {
                if task.project == project {
                    tasksData[selectedIndexPath!.row] = task
                    tableView.reloadRows(at: [selectedIndexPath!], with: .none)
                } else {
                    tasksData.remove(at: selectedIndexPath!.row)
                    tableView.deleteRows(at: [selectedIndexPath!], with: .none)
                }
            } else {
                let newIndexPath = IndexPath(row: tasksData.count, section: 0)
                tasksData.append(task)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
                print("Sauvegarde réussie")
            }
        }
        // Use data from the view controller which initiated the unwind segue
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "ShowTaskDetail":
            guard let taskDetailViewController = segue.destination as? TaskDetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedTask = tasksData[indexPath.row]
            taskDetailViewController.task = selectedTask
            taskDetailViewController.mode = .DETAIL
        case "unwindToProjectList":
            break
        case "showHistoric":
            guard let taskHistoryViewController = segue.destination as? HistoricTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let button = sender as? UIButton else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            let index = button.tag
            let selectedTask = tasksData[index]
            taskHistoryViewController.task = selectedTask
        case "presentTaskDetail":
            guard let modalNavigationController = segue.destination as? UINavigationController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let taskDetailViewController = modalNavigationController.viewControllers.first as? TaskDetailViewController else {
                fatalError("Unexpected root view controller: \(String(describing: modalNavigationController.viewControllers.first))")
            }
            taskDetailViewController.unwindSegueIdentifier = "unwindToProjectDetail"
            taskDetailViewController.parentProject = project
            taskDetailViewController.mode = .ADD
        default:
            fatalError("Unexpected Segue Identifier: \(String(describing: segue.identifier))")
        }
    }
    
    private func switchToEditMode() {
        mode = .EDIT
        tableView.isEditing = true
        navigationItem.rightBarButtonItem?.title = "Sauvegarder"
        navigationItem.leftBarButtonItem?.title = "Annuler"
        nameTextField.isUserInteractionEnabled = true
        nameTextField.becomeFirstResponder()
        nameTextField.layer.borderWidth = 0.25
        nameTextField.layer.cornerRadius = 5.0
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        tableView.reloadData()
    }
    
    private func switchToDetailMode() {
        mode = .DETAIL
        tableView.isEditing = false
        navigationItem.rightBarButtonItem?.title = "Modifier"
        navigationItem.leftBarButtonItem?.title = "Retour"
        nameTextField.isUserInteractionEnabled = false
        nameTextField.resignFirstResponder()
        nameTextField.layer.borderWidth = 0.25
        nameTextField.layer.cornerRadius = 5.0
        nameTextField.layer.borderColor = UIColor.clear.cgColor
    }
    
    private func switchToAddMode() {
        mode = Mode.ADD
        navigationItem.rightBarButtonItem?.title = "Sauvegarder"
        navigationItem.leftBarButtonItem?.title = "Annuler"
        nameTextField.isUserInteractionEnabled = true
        nameTextField.becomeFirstResponder()
        nameTextField.layer.borderWidth = 0.25
        nameTextField.layer.cornerRadius = 5.0
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        tableView.reloadData()
    }
}
