//
//  TasksTableViewController.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 15/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData

class TasksTableViewController: UITableViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
    
    var single: Bool = false
    
    var selectedIndexPath: IndexPath?
    
    struct TaskSection {
        var project: Project?
        var tasks: [Task]
    }
    
    var sections = [TaskSection]()
    
    enum Mode {
        case DETAIL
        case EDIT
    }
    var mode: Mode = Mode.DETAIL

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTasksData()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(TasksTableViewController.refreshTableView), userInfo: nil, repeats: true)
    }
    
    func loadTasksData() -> Void {
        let requestTasks : NSFetchRequest<Task> = Task.fetchRequest()
        requestTasks.predicate = NSPredicate(format: "project == nil")
        
        let requestProjects : NSFetchRequest<Project> = Project.fetchRequest()
        do {
            let tasksData = try context.fetch(requestTasks)
            
            if !single {
                let projectsData = try context.fetch(requestProjects)
                
                sections = projectsData.map { (project) in
                    let tasks = project.tasks?.allObjects as! [Task]
                    return TaskSection.init(project: project, tasks: tasks.sorted(by: {(t1, t2) in t1.name! < t2.name!}))
                }
                sections.sort(by: {
                    (ts1, ts2) in
                    if (ts1.project == nil) {
                        return true
                    }
                    if (ts2.project == nil) {
                        return false
                    }
                    return ts1.project!.name! < ts2.project!.name!
                })
            }
            
            sections.insert(TaskSection.init(project: nil, tasks: tasksData), at: 0)
        } catch {
            print("Les données n'ont pas pu être chargé")
        }
    }
    
    @objc func refreshTableView() {
        if !tableView.isEditing {
            tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sections[section].tasks.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let project = sections[section].project {
            return project.name
        } else {
            return "Tâches standalone"
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TaskCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TaskTableViewCell else {
            fatalError("La cellule n'est pas une instance de TaskTableViewCell")
        }
        cell.indexPath = indexPath
        let task = sections[indexPath.section].tasks[indexPath.row]
        let interval: TimeInterval
        if task.periods != nil && task.periods!.count > 0 {
            interval = task.getTotalTime()
        } else {
            interval = TimeInterval(0)
        }
        
        cell.nameLabel.text = task.name
        if tableView.isEditing {
            cell.timeLabel.text = ""
        } else {
            cell.timeLabel.text = interval.getTimeString()
        }
        if task.hasStarted() {
            cell.leftImageView.image = UIImage(named: "pause")
            cell.leftImageView.tintColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            cell.nameLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            cell.timeLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            
        } else {
            cell.leftImageView.image = UIImage(named: "play")
            cell.leftImageView.tintColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            cell.nameLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            cell.timeLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }
        cell.historyButton?.tag = indexPath.row
        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            context.delete(sections[indexPath.section].tasks[indexPath.row])
            sections[indexPath.section].tasks.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            do {
                try context.save()
            } catch {
                fatalError("La suppression a échoué")
            }
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        let task = sections[indexPath.section].tasks[indexPath.row]
        if task.quickTask {
            performSegue(withIdentifier: "presentQuickTaskMenu", sender: tableView.cellForRow(at: indexPath))
        } else {
            if mode == .EDIT {
                performSegue(withIdentifier: "ShowTaskDetail", sender: tableView.cellForRow(at: indexPath))
            } else {
                if task.hasStarted() {
                    task.stop()
                } else {
                    task.start()
                }
            }
        }
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let task = sections[fromIndexPath.section].tasks[fromIndexPath.row]
        let project = sections[to.section].project
        task.project = project
        sections[fromIndexPath.section].tasks.remove(at: fromIndexPath.row)
        sections[to.section].tasks.insert(task, at: to.row)
        
        do {
            try context.save()
            print("Tâche déplacée")
        } catch {
            print("Erreur durant le déplacement : \(error)")
        }
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    // MARK: Actions
    
    @IBAction func toggleEditMode(_ sender: Any) {
        if (tableView.isEditing) {
            mode = .DETAIL
            tableView.isEditing = false
            navigationItem.rightBarButtonItem?.title = "Modifier"
        }
        else {
            mode = .EDIT
            tableView.isEditing = true
            navigationItem.rightBarButtonItem?.title = "Terminer"
            tableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    @IBAction func unwindToTasks(_ unwindSegue: UIStoryboardSegue) {
        if let sourceViewController = unwindSegue.source as? TaskDetailViewController, let task = sourceViewController.task {
            if sourceViewController.mode == .DETAIL || sourceViewController.mode == .EDIT, selectedIndexPath != nil {
                let oldTask = sections[selectedIndexPath!.section].tasks[selectedIndexPath!.row]
                if task.project == oldTask.project {
                    sections[selectedIndexPath!.section].tasks[selectedIndexPath!.row] = task
                    tableView.reloadRows(at: [selectedIndexPath!], with: .none)
                } else {
                    let section = sections.firstIndex(where: {(ts) in return ts.project == task.project})
                    let newIndexPath = IndexPath(row: sections[section!].tasks.count, section: section!)
                    sections[selectedIndexPath!.section].tasks.remove(at: selectedIndexPath!.row)
                    sections[section!].tasks.append(task)
                    tableView.deleteRows(at: [selectedIndexPath!], with: .none)
                    tableView.insertRows(at: [newIndexPath], with: .automatic)
                }
            }
        }
        // Use data from the view controller which initiated the unwind segue
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "presentQuickTaskMenu":
            guard let quickTaskMenuViewController = segue.destination as? QuickTaskMenuViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedTask = sections[indexPath.section].tasks[indexPath.row]
            quickTaskMenuViewController.unwindSegueIdentifier = "unwindToTasks"
            quickTaskMenuViewController.quickTask = selectedTask
        case "ShowTaskDetail":
            guard let taskDetailViewController = segue.destination as? TaskDetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedTaskCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedTaskCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedTask = sections[indexPath.section].tasks[indexPath.row]
            taskDetailViewController.unwindSegueIdentifier = "unwindToTasks"
            taskDetailViewController.task = selectedTask
            taskDetailViewController.mode = .DETAIL
        case "unwindToProjectList":
            break
        case "showHistoric":
            guard let taskHistoryViewController = segue.destination as? HistoricTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let button = sender as? UIButton else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let cell = button.superview(of: TaskTableViewCell.self) else {
                fatalError("La cellule n'est pas du type voulue")
            }
            let indexPath = cell.indexPath!
            let selectedTask = sections[indexPath.section].tasks[indexPath.row]
            taskHistoryViewController.task = selectedTask
        default:
            fatalError("Unexpected Segue Identifier: \(String(describing: segue.identifier))")
        }
    }

}
