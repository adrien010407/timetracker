//
//  PeriodTableViewCell.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 11/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit

class PeriodTableViewCell: UITableViewCell {
    
    //MARK: Outlet
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
