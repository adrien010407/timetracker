//
//  QuickTaskMenuViewController.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 16/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData

class QuickTaskMenuViewController: UIViewController {
    var context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
    
    var unwindSegueIdentifier: String = "unwindToProjectList"
    var quickTask: Task?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let task = quickTask, var lastPeriod = task.getLastPeriod() {
            let currentDate = Date()
            let calendar = Calendar.current
            
            var startDate = lastPeriod.start!
            var endDay = calendar.date(bySetting: .nanosecond, value: 999999999, of: calendar.date(bySettingHour: 23, minute: 59, second: 59, of: startDate)!)
            while currentDate > endDay! {
                lastPeriod.end = endDay
                if #available(iOS 10.0, *) {
                    lastPeriod = Period(context: context)
                } else {
                    let entityDescription = NSEntityDescription.entity(forEntityName: "Period", in: context)
                    lastPeriod = Period(entity: entityDescription!, insertInto: context)
                }
                let newStartDate = calendar.date(byAdding: .day, value: 1, to: calendar.date(bySetting: .nanosecond, value: 0, of: calendar.date(bySettingHour: 0, minute: 0, second: 0, of: startDate)!)!)
                startDate = newStartDate!
                lastPeriod.start = newStartDate!
                task.addToPeriods(lastPeriod)
                endDay = calendar.date(bySetting: .nanosecond, value: 999999999, of: calendar.date(bySettingHour: 23, minute: 59, second: 59, of: startDate)!)
            }
            
            lastPeriod.end = Date()
        }

        // Do any additional setup after loading the view.
    }
    
    // MARK: Actions
    
    @IBAction func deleteTask(_ sender: Any) {
        context.delete(quickTask!)
        performSegue(withIdentifier: unwindSegueIdentifier, sender: sender)
    }
    
    @IBAction func cancelTask(_ sender: Any) {
        context.rollback()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "unwindToProjectList", "unwindToTasks":
            break
        case "presentTaskDetail":
            guard let modalNavigationController = segue.destination as? UINavigationController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let taskDetailViewController = modalNavigationController.viewControllers.first as? TaskDetailViewController else {
                fatalError("Unexpected root view controller: \(String(describing: modalNavigationController.viewControllers.first))")
            }
            taskDetailViewController.unwindSegueIdentifier = unwindSegueIdentifier
            taskDetailViewController.task = quickTask
            taskDetailViewController.parentProject = nil
            taskDetailViewController.mode = .ADD
        default:
            fatalError("L'identifier est inconnu: \(String(describing: segue.identifier))")
        }
    }
}
