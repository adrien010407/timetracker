//
//  UIView+.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 15/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit

extension UIView {
    func superview<T>(of type: T.Type) -> T? {
        return superview as? T ?? superview?.superview(of: type)
    }
}
