//
//  PeriodDetailViewController.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 12/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData

class PeriodDetailViewController: UIViewController {
    var context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
    
    var period: Period?
    
    enum PickerType {
        case UNKNOWN
        case START
        case END
        case INTERVAL
    }
    var pickerType: PickerType = PickerType.UNKNOWN
    
    //MARK: Outlet
    @IBOutlet weak var dateTimePicker: UIDatePicker!
    
    private var start: Date? = nil
    private var end: Date? = nil
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var intervalButton: UIButton!
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var intervalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let period = period, period.end != nil {
            start = period.start!
            startLabel.text = start!.description(with: Locale(identifier: "fr"))
            end = period.end!
            endLabel.text = end!.description(with: Locale(identifier: "fr"))
            let interval = end!.timeIntervalSince(start!)
            intervalLabel.text = interval.getTimeString()
        } else {
            fatalError("La période n'est pas défini ou celle-ci est toujours en cours")
        }
    }
    
    override func viewDidLayoutSubviews() {
        startButton.clipsToBounds = true
        let startPath = UIBezierPath(roundedRect: startButton.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10, height: 10))
        let startMaskLayer = CAShapeLayer()
        startMaskLayer.path = startPath.cgPath
        startButton.layer.mask = startMaskLayer
        
        intervalButton.clipsToBounds = true
        let intervalPath = UIBezierPath(roundedRect: startButton.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 10, height: 10))
        let intervalMaskLayer = CAShapeLayer()
        intervalMaskLayer.path = intervalPath.cgPath
        intervalButton.layer.mask = intervalMaskLayer
    }
    
    //MARK: Actions
    @IBAction func changeStart(_ sender: UIButton) {
        dateTimePicker.datePickerMode = .dateAndTime
        dateTimePicker.date = start!
        dateTimePicker.maximumDate = end!
        dateTimePicker.isHidden = false
        startButton.backgroundColor = UIColor(red: 0.2, green: 0.5, blue: 1.0, alpha: 1.0)
        endButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        intervalButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        startButton.setTitleColor(UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        endButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
        intervalButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
        
        startLabel.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        endLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        intervalLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        pickerType = .START
    }
    
    @IBAction func changeEnd(_ sender: UIButton) {
        dateTimePicker.datePickerMode = .dateAndTime
        dateTimePicker.date = end!
        dateTimePicker.minimumDate = start!
        dateTimePicker.maximumDate = Date()
        dateTimePicker.isHidden = false
        startButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        endButton.backgroundColor = UIColor(red: 0.2, green: 0.5, blue: 1.0, alpha: 1.0)
        intervalButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        startButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
        endButton.setTitleColor(UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        intervalButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
        
        startLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        endLabel.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        intervalLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        pickerType = .END
    }
    
    @IBAction func changeInterval(_ sender: UIButton) {
        dateTimePicker.datePickerMode = .countDownTimer
        let interval = end!.timeIntervalSince(start!)
        dateTimePicker.countDownDuration = interval
        dateTimePicker.isHidden = false
        startButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        endButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        intervalButton.backgroundColor = UIColor(red: 0.2, green: 0.5, blue: 1.0, alpha: 1.0)
        
        startButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
        endButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
        intervalButton.setTitleColor(UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        startLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        endLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        intervalLabel.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        pickerType = .INTERVAL
    }
    
    @IBAction func pickerValueChanged(_ sender: UIDatePicker) {
        switch pickerType {
        case .START:
            let newStartDate = sender.date
            let interval = end!.timeIntervalSince(start!)
            start = newStartDate
            end = newStartDate.addingTimeInterval(interval)
            startLabel.text = newStartDate.description(with: Locale(identifier: "fr"))
            endLabel.text = end!.description(with: Locale(identifier: "fr"))
            intervalLabel.text = end!.timeIntervalSince(start!).getTimeString()
        case .END:
            let newEndDate = sender.date
            end = newEndDate
            endLabel.text = newEndDate.description(with: Locale(identifier: "fr"))
            intervalLabel.text = end!.timeIntervalSince(start!).getTimeString()
        case .INTERVAL:
            let newInterval = sender.countDownDuration
            end = start!.addingTimeInterval(newInterval)
            intervalLabel.text = newInterval.getTimeString()
            endLabel.text = end!.description(with: Locale(identifier: "fr"))
        default:
            break
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        if touch?.view != dateTimePicker {
            dateTimePicker.isHidden = true
            startButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            endButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            intervalButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            startButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
            endButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
            intervalButton.setTitleColor(UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0), for: .normal)
            
            startLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            endLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            intervalLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            pickerType = .UNKNOWN
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "unwindToTaskHistoric" {
            period!.start = start!
            
            if var lastPeriod = period {
                let calendar = Calendar.current
                
                var startDate = lastPeriod.start!
                var endDay = calendar.date(bySetting: .nanosecond, value: 999999999, of: calendar.date(bySettingHour: 23, minute: 59, second: 59, of: startDate)!)
                while end! > endDay! {
                    lastPeriod.end = endDay
                    if #available(iOS 10.0, *) {
                        lastPeriod = Period(context: context)
                    } else {
                        let entityDescription = NSEntityDescription.entity(forEntityName: "Period", in: context)
                        lastPeriod = Period(entity: entityDescription!, insertInto: context)
                    }
                    let newStartDate = calendar.date(byAdding: .day, value: 1, to: calendar.date(bySetting: .nanosecond, value: 0, of: calendar.date(bySettingHour: 0, minute: 0, second: 0, of: startDate)!)!)
                    startDate = newStartDate!
                    lastPeriod.start = newStartDate!
                    period!.task!.addToPeriods(lastPeriod)
                    endDay = calendar.date(bySetting: .nanosecond, value: 999999999, of: calendar.date(bySettingHour: 23, minute: 59, second: 59, of: startDate)!)
                }
                
                lastPeriod.end = end!
            
                do {
                    try context.save()
                    print("Période sauvegardée")
                } catch {
                    print("Erreur durant la sauvegarde : \(error)")
                }
            }
        }
    }

}
