//
//  TaskDetailViewController.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 07/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData

class TaskDetailViewController: UIViewController {
    var context = (UIApplication.shared.delegate as! AppDelegate).databaseContext

    var unwindSegueIdentifier: String = "unwindToProjectDetail"
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var projectSelectButton: UIButton!
    @IBOutlet weak var projectSelectButtonArrowLabel: UILabel!
    
    var task: Task?
    var parentProject: Project?
    
    enum Mode {
        case UNKNOWN
        case ADD
        case DETAIL
        case EDIT
    }
    var mode: Mode = Mode.UNKNOWN

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if mode == .ADD {
            switchToAddMode()
        }
        else if mode == .DETAIL {
            switchToDetailMode()
        }
        else {
            fatalError("Le TaskDetailViewController n'est pas dans un mode connus")
        }
        
        if let task = task {
            navigationItem.title = task.name
            nameTextField.text = task.name
            parentProject = task.project
        }
        else {
            if #available(iOS 10.0, *) {
                task = Task(context: context)
            } else {
                let entityDescription = NSEntityDescription.entity(forEntityName: "Task", in: context)
                task = Task(entity: entityDescription!, insertInto: context)
            }
            task!.project = parentProject
        }
        projectSelectButton.setTitle(parentProject?.name ?? "Tâche standalone", for: .normal)
    }
    
    @IBAction func selectProject(_ sender: Any) {
        if mode == .EDIT || mode == .ADD {
            performSegue(withIdentifier: "presentProjectList", sender: sender)
        }
    }
    @IBAction func cancelOrReturn(_ sender: Any) {
        switch mode {
        case .ADD:
            dismiss(animated: true, completion: nil)
        case .DETAIL:
            performSegue(withIdentifier: unwindSegueIdentifier, sender: sender)
        case .EDIT:
            nameTextField.text = task!.name
            parentProject = task!.project
            projectSelectButton.setTitle(parentProject?.name, for: .normal)
            switchToDetailMode()
        default:
            fatalError("Le ProjectDetailViewController n'est pas dans un mode connu")
        }
    }
    @IBAction func saveOrEdit(_ sender: Any) {
        switch mode {
        case .ADD: // On sauvegarde un nouveau projet
            save()
            performSegue(withIdentifier: unwindSegueIdentifier, sender: sender)
        case .EDIT: // On sauvegarde une modification de projet
            save()
            switchToDetailMode()
        case .DETAIL: // On passe en mode édition
            switchToEditMode()
        default:
            fatalError("Le ProjectDetailViewController n'est pas dans un mode connu")
        }
    }
    //MARK: Fonctions
    private func save() {
        let name = nameTextField.text ?? "Tâche sans nom"
        
        task!.name = name
        task!.quickTask = false
        navigationItem.title = name
        task!.project = parentProject
        
        if !(task!.project?.objectID.isTemporaryID ?? false) {
            // Si le projet correspondant à la tâche a été sauvegardé, ou que la tâche est standalone, alors on sauvegarde la tâche, sinon on attend
            
            do {
                try context.save()
                print("Tâche sauvegardé")
            } catch {
                print("Erreur durant la sauvegarde : \(error)")
            }
            
        }
    }
    
    private func switchToEditMode() {
        mode = .EDIT
        navigationItem.rightBarButtonItem?.title = "Sauvegarder"
        navigationItem.leftBarButtonItem?.title = "Annuler"
        nameTextField.isUserInteractionEnabled = true
        nameTextField.becomeFirstResponder()
        nameTextField.layer.borderWidth = 0.25
        nameTextField.layer.cornerRadius = 5.0
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        projectSelectButton.isEnabled = true
        projectSelectButton.backgroundColor = UIColor(hue: 1.0, saturation: 0.0, brightness: 0.98, alpha: 1.0)
        projectSelectButtonArrowLabel.isHidden = false
    }
    
    private func switchToDetailMode() {
        mode = .DETAIL
        navigationItem.rightBarButtonItem?.title = "Modifier"
        navigationItem.leftBarButtonItem?.title = "Retour"
        nameTextField.isUserInteractionEnabled = false
        nameTextField.resignFirstResponder()
        nameTextField.layer.borderWidth = 0.25
        nameTextField.layer.cornerRadius = 5.0
        nameTextField.layer.borderColor = UIColor.clear.cgColor
        projectSelectButton.isEnabled = false
        projectSelectButton.backgroundColor = UIColor.clear
        projectSelectButtonArrowLabel.isHidden = true
    }
    
    private func switchToAddMode() {
        mode = Mode.ADD
        navigationItem.rightBarButtonItem?.title = "Sauvegarder"
        navigationItem.leftBarButtonItem?.title = "Annuler"
        nameTextField.isUserInteractionEnabled = true
        nameTextField.becomeFirstResponder()
        nameTextField.layer.borderWidth = 0.25
        nameTextField.layer.cornerRadius = 5.0
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        projectSelectButton.isEnabled = true
        projectSelectButton.backgroundColor = UIColor(hue: 1.0, saturation: 0.0, brightness: 0.98, alpha: 1.0)
        projectSelectButtonArrowLabel.isHidden = false
    }
    

    // MARK: - Navigation
    @IBAction func unwindToTaskDetail(_ unwindSegue: UIStoryboardSegue) {
        guard let sourceViewController = unwindSegue.source as? ProjectsTableViewController, let project = sourceViewController.selectedProject else {
            fatalError()
        }
        parentProject = project
         projectSelectButton.setTitle(parentProject?.name ?? "Tâche standalone", for: .normal)
    }

    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "unwindToProjectDetail", "unwindToProjectList", "unwindToTasks":
            break
        case "presentProjectList":
            guard let modalNavigationController = segue.destination as? UINavigationController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let projectsTableViewController = modalNavigationController.viewControllers.first as? ProjectsTableViewController else {
                fatalError("Unexpected root in navigation controller: \(String(describing: modalNavigationController.viewControllers.first))")
            }
            projectsTableViewController.selectedProject = parentProject
        default:
            fatalError("Unexpected Segue Identifier: \(String(describing: segue.identifier))")
        }
    }
    

}
