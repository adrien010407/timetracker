//
//  TimeInterval+.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 11/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit

extension TimeInterval {
    func getTimeString() -> String {
        let ti = NSInteger(self)
        let days = (ti / 86400)
        let hours = (ti / 3600) % 24
        let minutes = (ti / 60) % 60
        let seconds = ti % 60
        
        let time: String
        if days != 0 {
            time = String(format: "%dj %02d:%02d:%02d", days, hours, minutes, seconds)
        } else if hours != 0 {
            time = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            time = String(format: "%02d:%02d", minutes, seconds)
        }
        return time
    }
}
