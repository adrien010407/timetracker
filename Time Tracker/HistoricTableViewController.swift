//
//  HistoricTableViewController.swift
//  Time Tracker
//
//  Created by Adrien Boucher on 11/04/2020.
//  Copyright © 2020 Adrien Boucher. All rights reserved.
//

import UIKit
import CoreData

class HistoricTableViewController: UITableViewController {
    var context = (UIApplication.shared.delegate as! AppDelegate).databaseContext
    
    struct DaySection {
        var day: Date
        var periods: [Period]
    }
    
    var task: Task?
    var sections = [DaySection]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(HistoricTableViewController.refreshTableView), userInfo: nil, repeats: true)
    }
    
    func loadData() {
        let groups = Dictionary(grouping: task?.periods?.allObjects as! [Period]) {
            (period) in return getDay(date: period.start!)
        }
        
        sections = groups.map { (key, values) in
            return DaySection.init(day: key, periods: values.sorted(by: {(p1, p2) in p1.start! > p2.start!}))
        }
        sections.sort {
            (p1, p2) in p1.day > p2.day
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let section = sections[section]
        return section.periods.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let section = sections[section]
        let date = section.day
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PeriodCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PeriodTableViewCell else {
            fatalError("La cellule n'est pas une instance de PeriodTableViewCell")
        }
        let period = sections[indexPath.section].periods[indexPath.row]
        let interval = (period.end ?? Date()).timeIntervalSince(period.start!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        cell.periodLabel.text = "\(dateFormatter.string(from: period.start!)) - \(period.end != nil ? dateFormatter.string(from: period.end!) : "Maintenant")"
        cell.timeLabel.text = interval.getTimeString()
        
        if period.end == nil {
            cell.periodLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
            cell.timeLabel.textColor = UIColor(red: 0.6, green: 0.05, blue: 0.05, alpha: 1.0)
        } else {
            cell.periodLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            cell.timeLabel.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            context.delete(sections[indexPath.section].periods[indexPath.row])
            sections[indexPath.section].periods.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            do {
                try context.save()
            } catch {
                fatalError("La suppression a échoué")
            }
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showPeriodDetail" {
            guard let selectedPeriodCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedPeriodCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let period: Period = sections[indexPath.section].periods[indexPath.row]
            if period.end == nil {
                return false
            }
        }
        return true
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier ?? "" {
        case "showPeriodDetail":
            guard let periodDetailViewController = segue.destination as? PeriodDetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedPeriodCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = tableView.indexPath(for: selectedPeriodCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let period: Period = sections[indexPath.section].periods[indexPath.row]
            periodDetailViewController.period = period
        default:
            fatalError("Unexpected Segue Identifier: \(String(describing: segue.identifier))")
        }
    }
    
    @IBAction func unwindToTaskHistoric(_ unwindSegue: UIStoryboardSegue) {
        if ((unwindSegue.source as? PeriodDetailViewController) != nil) {
            loadData()
            tableView.reloadData()
        }
    }
    
    //MARK: Fonctions
    
    @objc func refreshTableView() {
        if !tableView.isEditing {
            tableView.reloadData()
        }
    }
    
    private func getDay(date: Date) -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        return calendar.date(from: components)!
    }

}
